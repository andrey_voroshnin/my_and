#include "my_and.h"

unsigned int var_and (int num_args, ...)
{
	unsigned int val;
	va_list ap;
	int i;

	va_start(ap, num_args);
	val = va_arg(ap, unsigned int);
	
	for(i = 1; i < num_args; i++) {
		val &= va_arg(ap, unsigned int);
	}
	va_end(ap);

	return val;
}

int main()
{
	
	printf("First run of function. Values are:\n");
	printf("1,11,111,1111,51,61,71,81,91,101\n");
	printf("Result of var_and is: %u\n",var_and(10,1,11,111,1111,51,61,71,81,91,101));
	
	printf("Second run of function. Values are:\n");
	printf("3758096383,4294967295,4290772991,4294967039,4227858431,4294705151,4294959103,4160749567,4294967287,4294967231\n");
	printf("Result of var_and is: %u\n",var_and(10,3758096383,4294967295,4290772991,4294967039,4227858431,4294705151,4294959103,4160749567,4294967287,4294967231));
	
	printf("Third run of function. Values are:\n");
	printf("201261054,268435455,234618879,268431343,264241151\n");
	printf("Result of var_and is: %u\n",var_and(5,201261054,268435455,234618879,268431343,264241151));
	
	printf("Fourth run of function. Values are:\n");
	printf("3870859,2096717\n");
	printf("Result of var_and is: %u\n",var_and(2,3870859,2096717));


	return 0;
}